import { Status } from './status';
export interface Week{
    name : string;
    regular: Status;
    diesel: Status;
    premium: Status;
}