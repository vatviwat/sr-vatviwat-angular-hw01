import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Week } from 'src/app/models/week';

@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit,OnChanges {

  // to get data from dashboard component
  @Input() week: Week

  totalRegular: number
  totalDiesel: number
  totalPremuim: number

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    // to calculate percent of regular
    this.totalRegular = this.week.regular.used / this.week.regular.available

    // to calculate percent of diesel
    this.totalDiesel = this.week.diesel.used / this.week.diesel.available

    // to calculate total percent of premuim
    this.totalPremuim = this.week.premium.used / this.week.premium.available
  }

  ngOnInit(): void {}
}
