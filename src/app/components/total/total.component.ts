import { Component, Input, OnInit } from '@angular/core';
import { Status } from 'src/app/models/status';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css'],
  providers: []
})
export class TotalComponent implements OnInit {

  @Input() total: Status[]
  totalRegular: number
  totalDiesel: number
  totalPremuim: number

  constructor() { }

  ngOnInit(): void {
    this.getTotalRegular();
    this.getTotalDiesel();
    this.getTotalPremuim();
  }
  
  // to calculate percent of regular 
  getTotalRegular(){
    return this.totalRegular = this.total[0].used/this.total[0].available*100
  }

  // to calculate percent of regular 
  getTotalDiesel(){
    return this.totalDiesel = this.total[1].used/this.total[1].available*100
  }

  // to calculate percent of 
  getTotalPremuim(){
    return this.totalPremuim = this.total[2].used/this.total[2].available*100
  }
}
