import { Component, OnInit } from '@angular/core';
import { Status } from 'src/app/models/status';
import { Week } from 'src/app/models/week';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // total data
  total: Status[]
  
  // branch A data
  week: Week

  constructor() { }

  ngOnInit(): void {
    this.generateTotalData();
    this.generateWeek();
  }

  // to generate random total data
  generateTotalData() {
    return this.total = [
      {
        used: this.generateRandomData(30), available: 30
      },
      {
        used: this.generateRandomData(20), available: 20
      },
      {
        used: this.generateRandomData(25), available: 25
      }
    ]
  }

  // to generate random week data
  generateWeek() {
    return this.week =
      {
        name: 'Week',
        regular: { used: this.generateRandomData(30), available: 30 },
        diesel: { used: this.generateRandomData(20), available: 20 },
        premium: { used: this.generateRandomData(25), available: 25 }
      }
  }

  // to generate random number
  generateRandomData(num:number){
    return Math.floor(Math.random() * Math.floor(num))
  }
}
