import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TotalComponent } from './components/total/total.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BranchComponent } from './components/branch/branch.component';
import { WeekRowComponent } from './components/week-row/week-row.component';

@NgModule({
  declarations: [
    AppComponent,
    TotalComponent,
    DashboardComponent,
    BranchComponent,
    WeekRowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
